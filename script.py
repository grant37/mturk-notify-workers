import boto
import argparse
from boto.mturk.connection import *

access_key="" # Your AWSAccessKeyId
secret_access_key="" # Your AWSSecretKey

conn = boto.mturk.connection.MTurkConnection(access_key, secret_access_key);

def main(subject, message, workers):
  error_count = 0;
  success_count = 0;
  total = 0;
  
  # Read in workerIDs into worker_ids list
  workers_file = open(workers, 'r');
  worker_ids = workers_file.readlines();
  workers_file.close();
  
  for worker in worker_ids:
    total += 1;
    print 'Progress: ['+str(total)+'/'+str(len(worker_ids))+']'
    
    worker = worker.rstrip();
    res = conn.notify_workers(worker, subject, message);
    if (len(res) > 0):
      # Error occured, log the workerID to error file
      error_count += 1;
      error_file = open("errors.txt", 'ab+');
      error_file.write(worker+'\n');
      error_file.close();
    else:
      # Success
      success_count += 1;
  
  print 'Finished: ['+str(success_count)+'/'+str(total)+'] successfull, '+str(error_count)+' errors';
  

parser = argparse.ArgumentParser(description='Send an email to a list of MTurk Workers');
parser.add_argument("workers_file", help="The file of MTurk worker ids.");
parser.add_argument("-s", "--subject", dest="subject", help="The subject for the email. If not specified, the program will prompt for this.");
parser.add_argument("-m", "--message", dest="message", help="The message for the email. If not specified, the program will prompt for this.");
args = parser.parse_args()

if (args.subject is None):
  args.subject = raw_input('Please enter the subject for the email: ');

if (args.message is None):
  args.message = raw_input('Please enter the message to be sent: ');
  
main(args.subject, args.message, args.workers_file);